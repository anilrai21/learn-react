import dispatcher from "../dispatchers/dispatcher.jsx";

export function createList(text) {
    dispatcher.dispatch({
        type: "CREATE_LIST",
        text,
    });
}