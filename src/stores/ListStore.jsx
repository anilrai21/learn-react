import { EventEmitter } from "events";
import dispatcher from "../dispatchers/dispatcher.jsx";

class ListStore extends EventEmitter {
    constructor() {
        super();
        this.lists = [
            {
                id: 1,
                text: "hello1"
            },
            {
                id: 2,
                text: "hello2"
            },
        ];
    }
    getAll() {
        return this.lists;
    }
    createList(text) {
        const id = Date.now();
        this.lists.push({
            id,
            text
        });
        this.emit("change");
    }
    handleActions(action) {
        console.log("Hello", action);
        switch(action.type) {
            case "CREATE_LIST": {
                this.createList(action.text);
                break;
            }
        }
    }
}
const listStore = new ListStore;
dispatcher.register(listStore.handleActions.bind(listStore));
window.dispatcher = dispatcher;
// window.listStore = listStore;
export default listStore;