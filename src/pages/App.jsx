"use strict";

import React from 'react';
import { BrowserRouter, Route, Link, hashHistory } from 'react-router-dom'

import Home from './Home.jsx';
import About from './About.jsx';
import List from './List.jsx';
import EasyChart from './EasyChart.jsx';
import D3 from './D3.jsx';
import Nav from '../components/Nav.jsx';
import ListStore from '../stores/ListStore.jsx';

export default class App extends React.Component {
    render() {
        return(
            <div>
                <BrowserRouter history={hashHistory}>
                    <div>
                        <Nav />
                        <Route path="/home" component={Home}></Route>
                        <Route path="/about" component={About}></Route>
                        <Route path="/list" component={List}></Route>
                        <Route path="/easychart" component={EasyChart}></Route>
                        <Route path="/d3" component={D3}></Route>
                    </div>
                </BrowserRouter>
            </div>
        );
    }
}