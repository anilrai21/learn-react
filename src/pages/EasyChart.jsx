import React from 'react';
import FirstBarChartComponent from '../components/FirstBarChartComponent.jsx';

export default class EasyChart extends React.Component {
    render() {
        return(
            <div>
                <h1>Easy Chart</h1>
                <FirstBarChartComponent/>
            </div>
        );
    }
}