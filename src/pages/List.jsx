"use strict";

import React from 'react';
import ListComponents from '../components/ListComponents.jsx';
import ListStore from '../stores/ListStore.jsx';
import * as ListActions from '../actions/ListActions.jsx';

export default class List extends React.Component {
    constructor() {
        super();
        this.state = {
            // lists: [
            //     {
            //         id: 1,
            //         text: "hello1"
            //     },
            //     {
            //         id: 2,
            //         text: "hello2"
            //     },
            // ]
            lists: ListStore.getAll(),
        }
    }
    componentWillMount() {
        ListStore.on("change", () => {
            this.setState({
                lists: ListStore.getAll(),
            });
        });
    }
    createList() {
        ListActions.createList(Date.now());
    }
    render() {
        // const lists = [
        //     {id:1, text:"first"},
        //     {id:2, text:"second"},
        //     {id:3, text:"third"},
        // ]
        let lists = this.state.lists;
        return(
            <div>
                {/* <ListComponents numbers={numbers} /> */}
                <button onClick={this.createList.bind(this)}>Add</button>
                <ListComponents lists={lists} />
            </div>
        );
    }
}