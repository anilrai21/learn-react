"use strict";

import React from 'react';
import { BrowserRouter, Route, Link } from 'react-router-dom';

import BarGraphExample from '../components/d3/BarGraphExample.jsx';

export default class D3 extends React.Component {
    
    render() {
        return(
            <div>
                <h1>D3</h1>
                <BrowserRouter>
                    <div>
                        <Link to="/bargraph">Bargraph</Link>
                        <Route path="/bargraph" component={BarGraphExample}></Route>
                    </div>
                </BrowserRouter>
            </div>
        );
    }
}