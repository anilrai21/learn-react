"use strict";

import React from 'react';
import { Link } from 'react-router-dom'

export default class Nav extends React.Component {
    render() {
        return(
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                {/* <a className="navbar-brand" href="">Navbar</a> */}
                <Link className="navbar-brand" to="/">My Site</Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item">
                            <Link className="nav-link" to="/home">Home</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/about">About</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/list">List</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/easychart">EasyChart</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/d3">D3</Link>
                        </li>
                    </ul>
                </div>
            </nav>
        );
    }
}