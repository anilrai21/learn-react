import React from 'react';
import ListItem from '../components/ListItem.jsx';

export default class ListComponents extends React.Component {
    render() {
        const lists = this.props.lists;
        const myList = lists.map((list) => 
            <ListItem key={list.id} list={list.text} />
        );
        return(
            <ul>
                {myList}
            </ul>
        )
    }
}