"use strict";

import React from 'react';
import { BarChart } from 'react-d3-components';

export default class BarGraphExample extends React.Component {
    constructor() {
        super();
        this.state = {
            data: [{
                label: 'somethingA',
                values: [{x: 'SomethingA', y: 10}, {x: 'SomethingB', y: 4}, {x: 'SomethingC', y: 3}]
            }]
        }
    }
    render() {
        return(
            <BarChart
                data={this.state.data}
                width={1000}
                height={500}
                margin={{top: 10, bottom: 50, left: 50, right: 10}}/>
        );
    }
}