import React from 'react';
import {BarChart} from 'react-easy-chart';

export default class FirstBarChartComponent extends React.Component {
    render() {
        return(
            <div>
                <h2>First Bar Chart</h2>
                <BarChart
                    // axisLabels={{x: 'My x Axis', y: 'My y Axis'}}
                    // axisLabels={{x: 'X-Axis', y: 'Y-Axis'}}
                    axes
                    colorBars
                    grid
                    height={250}
                    width={650}
                    interpolate={'cardinal'}
                    margin={{top: 0, right: 0, left: 100, bottom: 0}}
                    // xType={'linear'}
                    data={[
                        {x: 'A', y: 20},
                        {x: 'B', y: 30},
                        {x: 'C', y: 40},
                        {x: 'D', y: 20},
                        {x: 'E', y: 40},
                        {x: 'F', y: 25},
                        {x: 'G', y: 5},
                        {x: 'H', y: 5},
                        {x: 'I', y: 5},
                        {x: 'J', y: 20},
                        {x: 'K', y: 40},
                        {x: 'L', y: 25},
                        {x: 'M', y: 5},
                        {x: 'N', y: 5},
                        {x: 'O', y: 5},
                    ]}
                    lineData={[
                        {x: 'A', y: 20},
                        {x: 'B', y: 30},
                        {x: 'C', y: 40},
                        {x: 'D', y: 20},
                        {x: 'E', y: 40},
                        {x: 'F', y: 25},
                        {x: 'G', y: 5},
                        {x: 'H', y: 5},
                        {x: 'I', y: 5},
                        {x: 'J', y: 20},
                        {x: 'K', y: 40},
                        {x: 'L', y: 25},
                        {x: 'M', y: 5},
                        {x: 'N', y: 5},
                        {x: 'O', y: 5},
                    ]}

                />
            </div>
        );
    }
}