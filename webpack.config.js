const path = require('path');
const webpack = require('webpack');

var config = {
  entry: './src/index.js',
  output: {
    path: path.resolve('dist'),
    filename: 'bundle.js'
  },
  devServer: {
    port: 8000,
    historyApiFallback: true,
    hot: true
  },
  module: {
    loaders: [
      { test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/, query: {presets: ['es2015', 'react']} },
      { test: /\.jsx$/, loader: 'babel-loader', exclude: /node_modules/, query: {presets: ['es2015', 'react']}  },
    ]
  },
  plugins: [
    // Added Module Replacement Plugin for dev server. It will cause error if removed
    new webpack.HotModuleReplacementPlugin()
  ]
}
module.exports = config;